import "./sidebar.css";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
export default function Sidebar() {
  const [cats, setCats] = useState([]);

  useEffect(() => {
    const getCats = async () => {
      const res = await axios.get("/categories");
      setCats(res.data);
    };
    getCats();
  }, []);
  return (
    <div className="sidebar">
      <div className="sidebarItem">
        <span className="sidebarTitle">ABOUT ME</span>
        <img
          src="https://images.unsplash.com/photo-1572705824045-3dd0c9a47945?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NTJ8fGxpZmVzdHlsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60"
          alt=""
        />
        <p>"LifeStyle&More Blog"</p>
      </div>
      <div className="sidebarItem">
        <span className="sidebarTitle">CATEGORIES </span>
        <ul className="sidebarList">
          {cats.map((c) => (
            <Link to={"/?cat=${c.name}"} className="link">
              <li className="sidebarListItem">{c.name} </li>
            </Link>
          ))}
        </ul>
      </div>
      <div className="sidebarItem">
        <span className="sidebarTitle">FOLLOW US</span>
        <div className="sidebarSocial">
          <Link className="link" to="https://www.facebook.com/">
            <i className="topIcon fa-brands fa-facebook"></i>
          </Link>
          <Link className="link" to="https://www.twitter.com/">
            <i className="topIcon fa-brands fa-twitter-square"></i>
          </Link>
          <Link className="link" to="https://www.pinterest.com/pinterest/april-trends/" >
            <i className="topIcon fa-brands fa-pinterest"></i>
          </Link>
          <Link className="link" to="https://www.instagram.com/">
            <i className="topIcon fa-brands fa-instagram-square"></i>
          </Link>
        </div>
      </div>
    </div>
  );
}
