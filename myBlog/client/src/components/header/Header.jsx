import './header.css'

export default function Header() {
  return (
    <div className="header">
          <div className="headerTitles">
            <span className="headerTitleSm">Life & Style</span>
            <span className="headerTitleLg">Blog</span>
          </div>
          < img className="headerImg" src="https://images.unsplash.com/photo-1566501248434-6d513596c485?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" alt="profilepicture" />

    </div>
  )
}
